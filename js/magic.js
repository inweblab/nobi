$(document).ready(function(){
	// Mover y desaparecer texto counters Jquery.css
	// margin-top, opacity [0,1]

	var wow = new WOW({
		callback: function(box) {
			if ($(box).hasClass('the-counter')) {
					$('.counters').each(function () {
						$(this).prop('Counter', 0).animate(
							{
								Counter: $(this).text()
							}, 
							{
								duration: 4000,
								easing: 'swing',
								step: function (now) {
									$(this).text(Math.ceil(now));
								},
						});
					});
			}
		}
	});

	wow.init();

});
