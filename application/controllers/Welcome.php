<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = [
			'view' => 'home',
			'js' => [
				'/bower_components/slick-carousel/slick/slick.min.js',
				'/js/home.js',
			],
			'css' => [
				'/bower_components/slick-carousel/slick/slick-theme.css'
			],
			'tab' => 'home',
		];
		$this->load->view('master',$data);
	}
	
	public function convocatoria()
	{
		$data = [
			'js' => [],
			'css' => [],
			'view' => 'convocatoria',
			'tab' => 'convocatoria',
		];
		$this->load->view('master',$data);
	}

	public function participantes()
	{
		$data = [
			'js' => [],
			'css' => [],
			'view' => 'participantes',
			'tab' => 'participantes',
		];
		$this->load->view('master',$data);
	}

	public function leaders()
	{
		$data = [
			'js' => [],
			'css' => [],
			'view' => 'leaders',
			'tab' => 'lideres',
		];
		$this->load->view('master',$data);
	}

	public function cases()
	{
		$data = [
			'js' => [],
			'css' => [],
			'view' => 'cases',
			'tab' => 'casos',
		];
		$this->load->view('master',$data);
	}

	public function contact()
	{
		$data = [
			'js' => [],
			'css' => [],
			'view' => 'contact',
			'contact_page' => true,
			'tab' => 'contacto',
			
		];
		$this->load->view('master',$data);
	}
}
