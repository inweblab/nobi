<div class="container-fluid paddingless" id="home">

	<!-- Banner start -->
	<div class="container-fluid banner paddingless">
		<div class="row ">
			<div class="col text-center">
				<img class="logo" src="/img/layout/logos/logo_nobi_white.png" alt="NOBI" />
			</div>
		</div>
	</div>
	<!-- Banner end -->

	<!-- About start -->	
	<div class="container-fluid paddingless about">
		<div class="row">
			<div class="col text-center">
				<h1 class="wow fadeInUp m-t-120">¿Qué es NoBi MAP?</h1>
				<span class="divisor blue m-t-40 m-b-90"></span>
				<p>
					Es la propuesta del Nodo Binacional en Manufactura Avanzada y procesos
					la cual es liderada por el Centro de Investigación y Desarrollo Tecnológico
					en Electroquímica S.C., CIDETEQ e integra como instituciones socias 
					a diez Centros Públicos de Investigación e Instituciones de Educación
					Superior Privadas y Públicas.
				</p>
			</div>
		</div>
	</div>
	<!-- About end -->

	<!-- Objetvo start -->
	<div class="container p-t-70 objetive">
		<div class="row">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					Objetivo
				</h2>
				<span class="divisor blue m-t-30 m-b-30"></span>
				<div class="obj">
					<p class="block m-b-30 obj">
						Fortalecer las acciones de innovación con fines comerciales de las tecnologías desarrolladas
						en los Centros de I+D integrantes del Nodo, a través de la implementación de la 
						metodología de los Innovation Corps de la National Science Foundation.
					</p>
				</div>
				<h3>
					Los proyectos se desarrollan en cuatro sectores tecnológicos:
				</h3>
			</div>
		</div>

		<div class="row icons-grid m-t-30 p-b-50 wow fadeInUp">
			<div class="col-sm-12 col-md-3 project-column">
				<div class="icon text-center">
					<img src="/img/home/sector_1.png" alt="">
				</div>
				<h3 class="m-t-40 m-b-30">
					Procesos Sustentables
				</h3>
				<p>
					Manufactura Sustentable
					<br>
					Procesos Sustentable en Agua
					<br>
					Procesos Energéticos Sustentables
				</p>

			</div>
			<div class="col-sm-12 col-md-3 project-column">
				<div class="icon text-center">
					<img src="/img/home/sector_2.png" alt="">
					<h3 class="m-t-40 m-b-30">
						Materiales
					</h3>
					<p>
						Materiales Compuestos
						<br>
						Nanotecnología
						<br>
						Polímeros Avanzados
						<br>
						Procesamiento Avanzado de Materiales
					</p>
				</div>
			</div>
			<div class="col-sm-12 col-md-3 project-column">
				<div class="icon text-center">
					<img src="/img/home/sector_3.png" alt="">
					<h3 class="m-t-40 m-b-10">
						Robótica y Automatización
					</h3>
				</div>
			</div>
			<div class="col-sm-12 col-md-3 project-column">
				<div class="icon text-center">
					<img src="/img/home/sector_4.png" alt="">
				</div>
				<h3 class="m-t-40 m-b-10">
						Tecnologías de Información y Comunicación
					</h3>
			</div>
		</div>

	</div>
	<!-- Objetivo end -->

	<!-- Numeralia start -->
	<div class="container-fluid paddingless numeralia bg-numeralia">
		<div class="container p-t-50 p-b-50">
			<div class="row">
				<div class="col text-center">
					<h2 class="subheading lg wow fadeInUp the-counter">
						NoBi MAP: NUMERALIA 2018
					</h2>
					<span class="divisor blue m-t-40 m-b-40"></span>
				</div>
			</div>

			<div class="row icons-grid numeralia bg-numeralia">
				<div class="col">
					<div class="icon text-center">
						<div class="counter-ph">
							<img src="img/home/NUMERALIA_EQUIPOS.png" alt="">
						</div>
					</div>
					<div class="text-center">
						<span class="counters">
							23
						</span>
						<div class="text-under">
							<h3 class="black">
								EQUIPOS EN TOTAL
							</h3>
							<p>
								formados en la
								metodología “CORE”
								de I-CORPS, generado
								23 modelos de negocio
								registrados en la
								plataforma de
								Innovation Within.
							</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="icon text-center">
						<div class="counter-ph">
							<img src="img/home/NUMERALIA_PARTICIPANTES.png" alt="">	
						</div>
					</div>
					<div class="text-center">
							<span class="counters">
								7
							</span>
						<div class="text-under">
							<h3 class="black">
								INSTRUCTORES
							</h3>
							<p>
								Especializados y acreditados por el CONACYT y la NSF.
							</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="icon text-center">
						<div class="counter-ph">
							<img src="img/home/NUMERALIA_HORAS.png" alt="">	
						</div>
					</div>
					<div class="text-center">
							<span class="counters">
								100
							</span>
						<div class="text-under">
							<h3 class="black">
								HORAS
							</h3>
							<p>
								Más de cien horas de
								capacitación en temas
								de emprendimiento,
								validación de mercado y
								análisis de ciclo de vida.
							</p>
						</div>
					</div>
				</div>
				<div class="col">
					<div class="icon text-center">
						<div class="counter-ph">
							<img src="img/home/NUMERALIA_CASOS DE EXITO.png" alt="">	
						</div>
					</div>
					<div class="text-center">
							<span class="counters">
								8
							</span>
						<div class="text-under">
							<h3 class="black">
								CASOS DE ÉXITO
							</h3>
							<p>
								Proyectos de Base
								Tecnológica que
								generaron producto
								mínimo viable y validaron
								la aceptación con clientes
								potenciales en la feria
								Hannover 2018 y ferías de
								empredimiento como INC Monterrey.
							</p>
						</div>
					</div>
				</div>
			</div>

		</div>
		
	</div>
	<!-- Numeralia end -->

	<!-- Institutions logos start -->
	<div class="container-fluid paddingless institutions">
		<div class="container p-t-20">
			<div class="row">
				<div class="col">
					<div id="associates" class="slick-carousel">
						<img class="logo-carousel-img" src="/img/layout/logos/white/cideteq.png" alt="CIDETEQ">
						<img class="logo-carousel-img" src="/img/layout/logos/white/ciatec.png" alt="ciatec">
						<img class="logo-carousel-img" src="/img/layout/logos/white/cidesi.png" alt="cidesi">
						<img class="logo-carousel-img" src="/img/layout/logos/white/cinvestav.png" alt="cinvestav">
						<img class="logo-carousel-img" src="/img/layout/logos/white/ciqa.png" alt="ciqa">
						<img class="logo-carousel-img" src="/img/layout/logos/white/comimsa.png" alt="comimsa">
						<img class="logo-carousel-img" src="/img/layout/logos/white/itesm.png" alt="itesm">
						<img class="logo-carousel-img" src="/img/layout/logos/white/uteq.png" alt="uteq">
						<img class="logo-carousel-img" src="/img/layout/logos/white/upq.png" alt="upq">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Institutions logos end -->

	<!-- ICorps start -->
	<div class="container p-t-70 p-b-70 icorp">
		<div class="row">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					Modelo I-Corps
				</h2>
				<div class="modelo">
					<span class="divisor blue m-t-30 m-b-30"></span>
					<p>
						Es un programa creado por la NSF (National Science Foundation), lanzado en 2011 en los Estados Unidos, para facilitar
						la generación de empresas con tecnologías desarrolladas en laboratorios universitarios, abriendo espacios para la
						comercialización de tecnología a través de nuevos emprendimientos.
					</p>

					<p>
						El currículo de I-Corps fue desarrollado y es enseñado por emprendedores de base tecnológica. Cada participante en el
						programa I-Corps se involucrará ampliamente con la industria. En el transcurso de varias semanas, los participantes hablarán
						con un mínimo de cien clientes, socios, competidores y otras partes interesadas del mercado. Durante este tiempo, los
						participantes estarán buscando un sólido mercado de productos aptos y la construcción de un modelo de negocio viable para su innovación.
					</p>

					<p>
						A través de enseñanza de la metodología de I-Corps, los beneficiarios aprenden a identificar oportunidades de productos
						valiosos que pueden surgir de la investigación académica, y adquieren habilidades en emprendimiento a través de la
						capacitación en descubrimiento de clientes y orientación de empresarios establecidos.
					</p>
				</div>
			</div>
		</div>
		<div class="row mt-5 mb-5">
			<div class="col text-center">
				<img class="logo-icorps" src="/img/layout/logos/icorps_color.jpg" alt="I-Corps">
			</div>
		</div>
		<div class="row">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					¿Qué hace el programa I-Corps?
				</h2>
				<span class="divisor blue m-t-30 m-b-30"></span>
				<p>
					La currícula de I-Corps está basada en la metodología Lean Startup de Steven Blank y
					está adaptada al perfil de investigadores y centrada en el Descubrimiento del Cliente.
				</p>

				<p>
					Durante 7 u 8 semanas, los equipos recibirán capacitación en materia de comercialización de tecnologías de innovación.
					Cada equipo debe realizar al menos 100 entrevistas con clientes potenciales para entender mejor las necesidades del mercado,
					lo que los lleva a conocer de forma directa las problemáticas de su cliente potencial.
					Esto para complementar la información técnica desarrollada en los laboratorios y mejorar sus prototipos hasta obtener un
					producto mínimo viable que les permita iniciar su comercialización.
				</p>
				
			</div>
		</div>
	</div>
	<!-- ICorps end -->

	<!-- Equipos start -->
	<div class="container-fluid paddingless numeralia bg-numeralia">
		<div class="container pt-5 pb-5">
			<div class="row">
				<div class="col text-center">
					<h2 class="subheading wow fadeInUp">
						Equipos
					</h2>
					<span class="divisor blue m-t-30 m-b-30"></span>
					<h3 class="black">
						Todos los equipos estarán conformados por tres integrantes
					</h3>
				</div>
			</div>
			<div class="row m-t-50 equipos">
				<div class="col">
					<div class="text-center">
						<h2 class="subheading">
							Un investigador
						</h2>
						<p>
							principal, se desempeña como
							jefe técnico del proyecto y ha
							dirigido la investigación de la
							tecnología proponente.
						</p>
					</div>
				</div>
				<div class="col">
					<div class="text-center">
						<h2 class="subheading">
							Un estudiante (Líder emprendedor),
						</h2>
						<p>
							estudiante de pre o posgrado,
							posdoctoral o un técnico, con amplio
							conocimiento técnico en el campo de la
							tecnología a validar, es el responsable
							de la mayor carga de trabajo y de las
							actividades del programa.
						</p>
					</div>
				</div>
				<div class="col">
					<div class="text-center">
						<h2 class="subheading">
							Un mentor de negocios,
						</h2>
						<p>
							es un empresario o profesionista
							en el área de negocios o
							industrial, con experiencia
							en emprendimiento de base
							tecnológica y una amplia red de contactos.
						</p>
					</div>
				</div>
			</div>
			<div class="row icons-grid m-t-40">
				<div class="col">
					<div class="icon text-center">
						<img src="/img/home/technical.png" class="team-member" alt="Estudiante">
					</div>
				</div>
				<div class="col">
					<div class="icon text-center">
						<img src="/img/home/student.png" class="team-member" alt="Investigador">
					</div>
				</div>
				<div class="col">
					<div class="icon text-center">
						<img src="/img/home/mentor.png" class="team-member" alt="Mentor">
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Equipos end -->

	<!-- Video start -->
	<div class="container p-t-70 p-b-50">
		<div class="row">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					Conoce el Nobi MAP
				</h2>
				<span class="divisor blue m-t-30 m-b-30"></span>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<iframe width="100%" height="500" src="https://www.youtube.com/embed/FE4qcsN9tB8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
			</div>
		</div>

		

	</div>
	<!-- Video end -->

	<!-- Social bar start -->
	<div class="container-fluid paddingless numeralia bg-numeralia">
		<div class="container p-t-50 p-b-50">
			
			<div class="row">
				<div class="col">
					<nav class="social-bar social-land">
						<ul class="nav justify-content-center">
							<li class="nav-item align-middle">
								<a href="https://www.facebook.com/NobiMAP/" class="fb" target="_blank"></a>
							</li>    
							<li class="nav-item">
								<a href="https://twitter.com/nobimap" class="twitter" target="_blank"></a>
							</li>
							<li class="nav-item">
								<a href="https://mx.linkedin.com/company/nobi-map" class="linkedin" target="_blank"></a>
							</li>
							<li class="nav-item">
								<a href="https://www.youtube.com/channel/UCpM3XsucirrRbAR4T5QGEbA" class="youtube" target="_blank"></a>
							</li>
						</ul>
					</nav>
				</div>
			</div>

		</div>
		
	</div>
	<!-- Social bar end -->

	<!-- Nodos start -->
	<div class="container p-t-70 p-b-70">
		<div class="row m-b-50">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					Nodos Binacionales de Innovación
				</h2>
				<span class="divisor blue m-t-30 m-b-30"></span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-2 col-sm-12">
				<div class="card bg-numeralia node text-center">
					<a href="http://nobibajio.org/" target="_blank">
						<img src="/img/layout/logos/nodos/nobi_bajio.jpg" class="card-img-top" >
					</a>
					<div class="card-body text-center">
						<span class="divisor card blue m-t-30 m-b-40"></span>
						<p>
							Nodo Binacional de Innovación del Bajío
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="card bg-numeralia node text-center">
					<a href="#" target="_blank" rel="noopener noreferrer">
						<img src="/img/layout/logos/nodos/nobi_mar.png" class="card-img-top" >
					</a>
					<div class="card-body text-center">
						<span class="divisor card blue m-t-30 m-b-40"></span>
						<p>
							Nodo Binacional en Manufactura Avanzada y Procesos
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="card bg-numeralia node text-center">
					<a href="http://www.nobi-norte.org/" target="_blank">
						<img src="/img/layout/logos/nodos/nobi_norte.png" class="card-img-top" >
					</a>
					<div class="card-body text-center">
						<span class="divisor card blue m-t-30 m-b-40"></span>
						<p>
							Nodo Binacional de Innovación, Región Norte de México
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="card bg-numeralia node text-center">
					<a href="https://www.nodoinnovacionensalud.com/">
						<img src="/img/layout/logos/nodos/nobi_salud.png" class="card-img-top" >
					</a>
					<div class="card-body text-center">
						<span class="divisor card blue m-t-30 m-b-40"></span>
						<p>
							Nodo Binacional de Innovación en Salud
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="card bg-numeralia node text-center">
					<a href="http://www.nobiu.unam.mx/">
						<img src="/img/layout/logos/nodos/nobi_universitario.jpg" class="card-img-top" >
					</a>
					<div class="card-body text-center">
						<span class="divisor card blue m-t-30 m-b-40"></span>
						<p>
							Nodo Binacional de Innovación Universitario
						</p>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-12">
				<div class="card bg-numeralia node text-center">
					<a href="http://nobisureste.mx/">
						<img src="img/layout/logos/nodos/nobi_sureste.png" class="card-img-top" >
					</a>
					<div class="card-body text-center">
						<span class="divisor card blue m-t-30 m-b-40"></span>
						<p>
							Nodo Binacional de Innovación del Sureste
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Nodos end -->

	<!-- Contact start -->
	<?php $this->load->view('partials/contact') ?>
	<!-- Contact end -->


</div>
