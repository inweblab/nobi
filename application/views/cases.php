<div class="container-fluid paddingless p-b-70" id="cases">
	<!-- Title start -->
	<div class="container p-t-70">
		<div class="row">
			<div class="col text-center">
				<h1 class="wow fadeInUp">Casos de éxito</h1>
			</div>
		</div>
	</div>
	<!-- Title end -->
	
	<!-- Case start -->
	<div class="container-fluid paddingless m-t-45">

		<div class="container-fluid paddingless bg-numeralia ">
			<!-- Title -->
			<div class="row">
				<div class="col text-center p-t-25 p-b-20">
					<h2 class="wow fadeInUp">
						CIATHULE Recycled Tire rubber
				</div>
			</div>
			<!-- Title -->
		</div>

		<!-- Content -->
		<div class="row case m-t-80">
			<div class="col">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
						<img src="/img/cases/ciatec/logo.png" alt="CIATEC" class="logo wow fadeInUp">
						<p> Equipo 19 </p>	
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7 col-xl-7">
						<div class="text-intro">
							<p>
								Uno de los proyectos más avanzados en su negociación se presentó en Hannover Messe.
							</p>
							<p>
								A CIATEC le interesa participar de este emprendimiento como accionista. Se presentó en Hannover
								Messe, tiene el interés de CONTINENTAL para madurar prueba de concepto bajo los criterios de
								esta empresa, una de las más importantes para la comercialización de autopartes entre México y
								Alemania, y de demostrarse positiva, con posibilidades de compra.
							</p>
						</div>
					</div>
				</div>
				<div class="row m-t-60 cases-images-container wow fadeInUp">
					<div class="col first-imagen">
						<img src="/img/cases/ciatec/1.png" alt="" class="">
					</div>
					<div class="col esp1">
						<img src="/img/cases/ciatec/2.png" alt="" class="">
					</div>
					<div class="col esp2">
						<img src="/img/cases/ciatec/3.png" alt="" class="">
					</div>
					<div class="col esp3">
						<img src="/img/cases/ciatec/4.png" alt="" class="">
					</div>	
				</div>
				
			</div>
		</div>
		<!-- Content -->

		<!-- Social -->
		<div class="row">
			<div class="col">
				<nav class="social-bar casos-exito">
					<ul class="nav justify-content-center mod">
						<li class="nav-item">
							<a href="https://www.facebook.com/ciatec.mx/" class="fb"></a>
						</li>    
						<li class="nav-item">
							<a href="https://twitter.com/ciatec_mx" class="twitter"></a>
						</li>
						<li class="nav-item">
							<a href="https://mx.linkedin.com/company/ciatec" class="linkedin"></a>
						</li>
						<li class="nav-item">
							<a href="https://www.youtube.com/watch?v=tt5cXExMrbg" class="youtube"></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- Social -->

		<!-- Testimony -->
		<div class="row testimonial">
			<div class="col-md-10 offset-md-1 text-center">
				<p>
					El Nodo Binacional del CONACYT NSF ha sido una gran experiencia. La participación de un servidor en el
					NOBI de Materiales Avanzados, coordinado por el CIDETEQ, ha sido la puerta de acceso al conocimiento
					acerca de la realidad de mercado de algunas de las tecnologías desarrolladas en CIATEC, no obstante, la
					metodología es aplicable a cualquiera. Ahora sabemos la estrategia y actividades requeridas para saber
					si realmente una tecnología tiene valor en el mercado, o es simplemente un proyecto científico con el que

					el investigador puede publicar. El CIATEC aplicó la experiencia en tres proyectos tecnológicos, los cua-
					les fueron llevados hasta la etapa de incubación, alcanzando el desarrollo del Modelo de Negocio, Plan

					de Negocio, y Plan Financiero, requisitos indispensables para el establecimiento de una empresa de base
					tecnológica. El plan futuro es aplicarlo a más tecnologías que nos lleven al establecimiento de EBT ́s, o

					bien, a realmente transferir las tecnologías.
				</p>
				<p class="blue_2">
					DR. SERGIO ALONSO ROMERO
					<br>
					DIRECCIÓN DE INVESTIGACIÓN, POSGRADO Y CAPACITACIÓN
				</p>
			</div>
		</div>
		<!-- Testimony -->

	</div>
	<!-- Case end -->

	<!-- Case start -->
	<div class="container-fluid paddingless m-t-80">
		
		<div class="container-fluid paddingless bg-numeralia">
			<!-- Title -->
			<div class="row">
				<div class="col text-center p-t-25 p-b-20">
					<h2 class="wow fadeInUp">
						Electrofarming
				</div>
			</div>
			<!-- Title -->
		</div>

		<!-- Content -->
		<div class="row case m-t-80">
			<div class="col">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
						<img src="/img/cases/cideteq/logo.png" alt="cideteq" class="logo wow fadeInUp">
						<p > Equipo 14 </p>	
						
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7 col-xl-7">
						<div class="text-intro">
							<p>
								Uno de nuestros proyectos con mayor potencial para el emprendimiento de base tecnológica.
							</p>

							<p>
								Hay un inversionista interesado, contactado en el evento de Mentor Hub en Querétaro.
								Presentó su pitch en INC Monterrey.
							</p>

							<p>
								La doctora Erika Bustos ha expresado su gusto por este tipo de experiencias, lo formativo y
								diferente de la misma, y la oportunidad para buscar ahora proyectos con una salida de corto y
								mediano plazo a mercado. Se desarrolló el diseño de un producto mínimo viable para su
								validación.
							</p>
						</div>
					</div>
				</div>
				<div class="row m-t-60 cases-images-container wow fadeInUp">
					<div class="col first-imagen">
						<img src="/img/cases/cideteq/1.png" alt="" class="">
					</div>
					<div class="col esp1">
						<img src="/img/cases/cideteq/2.png" alt="" class="">
					</div>
					<div class="col esp2">
						<img src="/img/cases/cideteq/3.png" alt="" class="">
					</div>
					<div class="col esp3">
						<img src="/img/cases/cideteq/4.png" alt="" class="">
					</div>
				</div>

			</div>
		</div>
		<!-- Content -->

		<!-- Social -->
		<div class="row">
			<div class="col">
				<nav class="social-bar casos-exito">
					<ul class="nav justify-content-center mod">
						<li class="nav-item">
							<a href="https://www.facebook.com/CIDETEQSC" class="fb"></a>
						</li>    
						<li class="nav-item">
							<a href="https://twitter.com/cideteq" class="twitter"></a>
						</li>
						<li class="nav-item">
							<a href="https://mx.linkedin.com/company/cideteq-sc" class="linkedin"></a>
						</li>
						<li class="nav-item">
							<a href="https://www.youtube.com/watch?v=SaEqZwQFcn8" class="youtube"></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- Social -->

		<!-- testimonial -->
		<div class="row testimonial">
			<div class="col-md-10 offset-md-1 text-center">
				<p>
					“La experiencia que tuve en la primera generación del Nodo Binacional de Manufactura Avanzada
					de la NSF - CONACyT - SINERTEC fue realmente satisfactoria, lo cual recomiendo ampliamente a
					quien tenga la oportunidad de involucrarse al respecto, ya que superaron mis expectativas todo el
					equipo de trabajo que integraron para vivir esta experiencia, pues nos permitieron analizar el ciclo
					de vida, estudio de mercado, modelo de negocio, financiero y legal de los proyectos de
					investigación que teníamos a nivel de laboratorio y piloto, logrando sensibilizarnos tanto a
					estudiantes como a investigadores de la importancia que tiene el integrar conceptos
					administrativos a nuestras bases de ciencia fundamental para ofrecer un proyecto de gran
					envergadura a la sociedad nacional con proyección internacional, con el fin de fomentar la
					innovación para la incubación de empresas de base tecnológica”.
				</p>
				<p class="blue_2">
					DRA. ERIKA BUSTOS BUSTOS
					<br>
					INVESTIGADORA TITULAR, DIRECCIÓN DE CIENCIA
				</p>
			</div>
		</div>
		<!-- testimonial -->

	</div>
	<!-- Case end -->

	<!-- Case start -->
	<div class="container-fluid paddingless m-t-80">
		
		<div class="container-fluid bg-numeralia paddingless">
			<!-- Title -->
			<div class="row">
				<div class="col text-center p-t-25 p-b-20">
					<h2 class="wow fadeInUp">
						Antimicrobial threads for textile use
				</div>
			</div>
			<!-- Title -->
		</div>

		<!-- Content -->
		<div class="row case m-t-80">
			<div class="col">
				<div class="row">
					<div class="col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
						<img src="/img/cases/ciqa/logo.png" alt="CIATEC" class="logo wow fadeInUp">
						<div class="text-center">
							<p> Equipo 8 </p>	
						</div>
					</div>
					<div class="col-sm-12 col-md-7 col-lg-7 col-xl-7">
						<div class="text-intro">
							<p>
								Durante el proceso de “customer discovery”, se conceptualizaron dos propuestas de valor
								centrales para la tecnología de inclusión de nanopartículas de cobre, tanto en polímeros como en
								superficies metálicas. En ambos casos, se desarrollaron planes y escenarios para la transferencia
								tecnológica.
							</p>
							<p>
								Este proyecto fue seleccionado para ser presentado en Hannover Messe.
							</p>
							<p>
								Los planes de negocio se han desarrollado, y existe interés para el emprendimiento de base
								tecnológica por parte de CIQA y el equipo de emprendimiento.
							</p>
						</div>
					</div>
				</div>
				<div class="row m-t-60 cases-images-container wow fadeInUp">
					<div class="col first-imagen">
						<img src="/img/cases/ciqa/1.png" alt="" class=" ">
					</div>
					<div class="col esp1">
						<img src="/img/cases/ciqa/2.png" alt="" class="">
					</div>
					<div class="col esp2">
						<img src="/img/cases/ciqa/3.png" alt="" class="">
					</div>
					<div class="col esp3">
						<img src="/img/cases/ciqa/4.png" alt="" class="">
					</div>
				</div>

			</div>
		</div>
		<!-- Content -->

		<!-- Social -->
		<div class="row">
			<div class="col">
				<nav class="social-bar casos-exito">
					<ul class="nav justify-content-center mod">
						<li class="nav-item">
							<a href="https://www.facebook.com/CIQA-249127805231213/" class="fb"></a>
						</li>    
						<li class="nav-item">
							<a href="https://twitter.com/ciqa_comunica" class="twitter"></a>
						</li>
						<li class="nav-item">
							<a href="https://mx.linkedin.com/company/ciqa" class="linkedin"></a>
						</li>
						<li class="nav-item">
							<a href="https://www.youtube.com/watch?v=P-3Xll5C2wc" class="youtube"></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		<!-- Social -->

		<!-- testimonial -->
		<div class="row testimonial">
			<div class="col-md-10 offset-md-1 text-center">
				<p>
					Mi experiencia en el NoBi abrió mi visión de la tecnología desarrollada en el grupo de
					investigación.
					Expandió mis horizontes para ver la tecnología como un negocio de la generación de
					conocimiento.
				</p>
				<p class="blue_2">
					DR. CARLOS ÁVILA
				</p>
			</div>
		</div>
		<!-- testimonial -->

	</div>
	<!-- Case end -->
	<div class="bot"></div>
</div>
