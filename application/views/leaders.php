<div class="container-fluid paddingless" id="leaders">
	<!-- Content start -->
	<div class="container p-t-70 p-b-70">
		<!-- Description start -->
		<div class="row">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					LÍDERES NoBi MAP
				</h2>
				<span class="divisor blue m-t-30 m-b-30"></span>
				
			</div>
		</div>
		<!-- Description end-->

		<!-- Row start -->
		<div class="row m-b-30">
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp " src="/img/layout/logos/socias/CIATEC.png" alt="CIATEC">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									Dr. Sergio Alonso<br>
									Líder Institucional CIATEC<br>
									salonso@ciatec.mx<br>
									(447) 7100011 ext. 13005
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp square-img" src="/img/layout/logos/socias/CIATEQ.png" alt="CIATEQ">
							</div>
							<div class="col-sm-8 p-t-5">
								<p class="">
									M en C. Hilda Hernández<br>
									Líder Institucional CIATEQ<br>
									hilda.hernandez@ciateq.mx<br>
									(442)2112600 ext. 2644
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row start -->
		<div class="row m-b-30">
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp rectangle-img" src="/img/layout/logos/socias/CIDESI.png" alt="CIDESI" style="">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									M en C. Enith Fuentes Martínez<br>
									Líder Institucional CIDESI<br>
									enith.fuentes@cidesi.edu.mx<br>
									(442) 2119800
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<div class="card-body especial">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp" src="/img/layout/logos/socias/CINVESTAV.png" alt="CINVESTAV" style="width:70%; margin-top:9px;">
							</div>
							<div class="col-sm-8">
								<p>
									CP. Alfonso Reynoso<br>
									Líder Institucional CINVESTAV<br>
									Unidad Querétaro.<br>
									areynoso@cinvestav.mx<br>
									(442) 2119900
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row start -->
		<div class="row m-b-30">
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp rectangle-img" src="/img/layout/logos/socias/CIQA.png" alt="CIQA">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									M en C. Herminia Cerda<br>
									Líder Institucional CIQA<br>
									hermina.cerda@ciqa.edu.mx<br>
									(442) 2119800
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp square-img" src="/img/layout/logos/socias/COMIMSA.png" alt="COMIMSA" style="width:70%;">
							</div>
							<div class="col-sm-8 p-t-7">
								<p>
									M en C. Ada Dávila<br>
									Líder Institucional COMIMSA<br>
									apdavila@comimsa.com<br>
									(844)4113201
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row start -->
		<div class="row m-b-30">
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp square-img" src="/img/layout/logos/socias/ITQ.png" alt="ITQ" style="width:70%;">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
								MBA. Victoria Cisneros <br>
								Líder Institucional ITQ <br>
								vcisneros@mail.itq.edu.mx <br>
								(442) 2274414
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp rectangle-img" src="/img/layout/logos/socias/ITESM.png" alt="ITESM" style="">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									LEM. Patricia Barrera<br>
									Líder Institucional ITESM<br>
									pbarrera@itesm.mx<br>
									442 238-33383
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row start -->
		<div class="row m-b-30">
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp square-img" src="/img/layout/logos/socias/UPQ.png" alt="UPQ" style="width:70%;">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									Dr. Cesar Izasa<br>
									Líder Institucional UPQ<br>
									cesar.isaza@upq.mx<br>
									(442) 101 9000 ext. 334
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp rectangle-img prec" src="/img/layout/logos/socias/UTEQ.png" alt="UTEQ" style="">
							</div>
							<div class="col-sm-8 p-t-20">
								<p>
									M en C. Marianela Talavera<br>
									Líder Institucional UTEQ<br>
									marianela.talavera@uteq.edu.mx<br>
									
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row start -->
		<div class="row m-b-30">
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp rectangle-img prec" src="/img/layout/logos/socias/CIDETEQ.png" alt="CIDETEQ">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									Dr. Yunny Meas<br>
									Responsable Técnico<br>
									NoBi MAP<br>
									Nobimap0@gmail.com
								</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-4 text-center">
								<img class="wow fadeInUp rectangle-img prec" src="/img/layout/logos/socias/CIDETEQ.png" alt="CIDETEQ">
							</div>
							<div class="col-sm-8 p-t-5">
								<p>
									MLEM. Carolina Tenorio<br>
									Líder Institucional CIDETEQ<br>
									ctenorio@cideteq.mx<br>
									2116000 ext. 7840
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Row end -->



	</div>
	<!-- Content end -->

	<!-- Banner start -->
	<div class="container-fluid banner bottom paddingless">
	</div>
	<!-- B anner end -->
	
</div>
