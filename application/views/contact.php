<div class="container-fluid paddingless p-b-70" id="contact">
	<div class="bg-image"></div>
	<!-- Title start -->
	<div class="container p-t-70">
		<div class="row">
			<div class="col text-center">
				<h1>Contacto</h1>
			</div>
		</div>

		<?php $this->load->view('partials/contact'); ?>

	</div>
	<!-- Title end -->
</div>
