<div class="container-fluid paddingless" id="convocatoria">
	<!-- Banner start -->
	<div class="container-fluid banner top paddingless">
	</div>
	<!-- B anner end -->

	<div class="container p-t-70 p-t-70">
		<div class="row">
			<div class="col text-center">
				<h1 class="wow fadeInUp">
					Convocatoria NoBi Map2019
				</h1>
			</div>
		</div>
	</div>

	<!-- Contact start -->
	<?php $this->load->view('partials/contact') ?>
	<!-- Contact end -->

	<!-- Banner start -->
	<div class="container-fluid banner bottom paddingless">
	</div>
	<!-- B anner end -->

</div>
