<div class="container-fluid paddingless" id="participants">
	<!-- Content start -->
	<div class="container p-t-70 p-b-70">
		<!-- Description start -->
		<div class="row">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp initial">
					INSTITUCIONES SOCIAS
				</h2>
				<span class="divisor blue m-t-30 m-b-30"></span>
				<p class="block m-b-30">
					Las Instituciones Socias buscan fortalecer la creación de un lazo de colaboración que les permita
					potenciar las capacidades y estrategias en innovación, emprendimiento, transferencia tecnológica y acercamiento al mercado.
				</p>
			</div>
		</div>
		<!-- Description end-->

		<!-- Leader start -->
		<div class="row m-t-50 m-b-50">
			<div class="col text-center">
				<h2 class="subheading wow fadeInUp">
					CIDETEQ (Institución Líder):
				</h2>
				<div >
					<a href="https://www.cideteq.mx/" target="_blank">
						<img src="/img/layout/logos/socias/CIDETEQ.png" alt="CIDETEQ" class="m-t-50 leader wow fadeInUp">
						<h3 class="gray m-t-30">
							<b>Centro de Investigación y Desarrollo <br> Tecnológico en Electroquímica</b>
						</h3>
					</a>	
				</div>
			</div>
		</div>
		<!-- Leader end -->

		<!-- Row end -->
		<div class="row">
			<div class="col">
				<div class="card shadow">
					<a href="https://www.ciatec.mx" target="_blank">
						<img src="/img/layout/logos/socias/CIATEC.png" alt="CIATEC" class="m-t-50 card-img-top wow fadeInUp">
						<div class="card-body text-center">
							<p class="gray m-t-30">
								<b>Centro de Innovación Aplicada en Tecnologías Competitivas</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="https://www.ciateq.mx" target="_blank">
						<img src="/img/layout/logos/socias/CIATEQ.png" alt="CIATEQ" class="m-t-50 card-img-top wow fadeInUp" >
						<div class="card-body text-center">
							<p class="gray m-t-50">
								<b>Centro de Tecnología <br> Avanzada</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="https://www.cidesi.com" target="_blank">
						<img src="/img/layout/logos/socias/CIDESI.png" alt="CIDESI" class="m-t-50 card-img-top wow fadeInUp" style="margin-top:100px !important;">
						<div class="card-body text-center">
							<p class="gray m-t-80">
								<b>Centro de Ingeniería y Desarrollo Industrial</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="https://www.cinvestav.mx" target="_blank">
						<img src="/img/layout/logos/socias/CINVESTAV.png" alt="CINVESTAV" class="m-t-50 card-img-top wow fadeInUp" style="width:49%">
						<div class="card-body text-center">
							<p class="gray m-t-30">
								<b>Centro de Investigación y de Estudios Avanzados</b>
							</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row end -->
		<div class="row">
			<div class="col">
				<div class="card shadow">
					<a href="https://www.ciqa.mx" target="_blank">
						<img src="/img/layout/logos/socias/CIQA.png" alt="CIQA" class="m-t-50 card-img-top wow fadeInUp" style="margin-top:100px !important;">
						<div class="card-body text-center">
							<p class="gray m-t-30">
								<b>Centro de Investigación en Química Aplicada</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="https://www.comimsa.com.mx" target="_blank">
						<img src="/img/layout/logos/socias/COMIMSA.png" alt="COMIMSA" class="m-t-50 card-img-top wow fadeInUp" style="width:45%;">
						<div class="card-body text-center">
							<p class="gray m-t-20">
								<b>Corporación Mexicana de Investigación en Materiales</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="http://www.itq.edu.mx" target="_blank">
						<img src="/img/layout/logos/socias/ITQ.png" alt="ITQ" class="m-t-50 card-img-top wow fadeInUp" style="width:39%;">
						<div class="card-body text-center">
							<p class="gray m-t-20">
								<b>Instituto Tecnológico de Querétaro</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="https://tec.mx/es" target="_blank">
						<img src="/img/layout/logos/socias/ITESM.png" alt="ITESM" class="m-t-50 card-img-top wow fadeInUp" style="margin-top:80px !important;">
						<div class="card-body text-center">
							<p class="gray m-t-30">
								<b>Instituto Tecnológico y de Estudios Superiores de Monterrey</b>
							</p>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Row end -->

		<!-- Row end -->
		<div class="row">
			<div class="col"></div>
			<div class="col">
				<div class="card shadow">
					<a href="https://www.uteq.edu.mx" target="_blank">
						<img src="/img/layout/logos/socias/UTEQ.png" alt="UTEQ" class="m-t-50 card-img-top wow fadeInUp" style="margin-top:70px !important;">
						<div class="card-body text-center">
							<p class="gray m-t-20">
								<b>Universidad Tecnológica de Querétaro</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col">
				<div class="card shadow">
					<a href="https://www.upq.mx" target="_blank">
						<img src="/img/layout/logos/socias/UPQ.png" alt="UPQ" class="m-t-50 card-img-top wow fadeInUp" style="width:39%;">
						<div class="card-body text-center">
							<p class="gray m-t-20">
								<b>Universidad Politénica de Querétaro</b>
							</p>
						</div>
					</a>
				</div>
			</div>
			<div class="col"></div>
		</div>
		<!-- Row end -->

	</div>
	<!-- Content end -->

	<!-- Banner start -->
	<div class="container-fluid banner bottom paddingless">
	</div>
	<!-- B anner end -->
	
</div>
