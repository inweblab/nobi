<?php $this->load->view('partials/head'); ?>
<body>
	<?php $this->load->view('partials/header'); ?>
	<div id="content">
		<?php $this->load->view($view); ?>
	</div>
	<?php $this->load->view('partials/footer'); ?>
	<script src="/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<?php foreach($js as $script): ?>
	<script src="<?php echo $script; ?>"></script>
	<?php endforeach;?>
	<script src="/bower_components/wow/dist/wow.min.js"></script>
	<script src="/js/magic.js"></script>
	
</body>
