<div class="container p-t-40 p-b-50" id="contact-area">
	<?php if(!isset($contact_page)): ?>
	<div class="row">
		<div class="col text-center">
			<h1 class="black font-weight-bold wow fadeInUp">
				Contacto
			</h1>
			<h3 class="black">
				Para más información
			</h3>
		</div>
	</div>
	<div class="row m-t-30">
		<div class="col text-center">
			<p>
				Nodo Binacional en Manufactura
				Avanzada y Procesos Industriales
			</p>
			<p>
				Parque San Fandila s/n, Pedro Escobedo, 76703
				Santiago de Querétaro, Qro.
			</p>
			<p>
				Horario de atención:
				<br>
				L – V : 8:30 hrs a 14:00 hrs
				<br>
				15:00 hrs a 17:00 hrs<br>
				Tel +52 (442) 2116000 ext 7840
				<br>
				Whatsapp: 442 364 5725
			</p>
			<p>
				E-mail. Carolina Tenorio Morales<br>
				ctenorio@cideteq.mx
			</p>
			<p class="font-weight-bold blue_1">
				Si quieres participar en la convocatoria<br>
				2019 envíanos tus datos
			</p>
		</div>
	</div>
	<?php endif; ?>
	<div class="row m-t-70 m-b-70">
		<div class="col">
			<form action="#" method="post" class="contact-form bg-numeralia shadow-lg p-r-70 p-l-70 p-t-70 p-b-70 wow fadeIn">
				
				<div class="form-group">
					<label for="name">Nombre</label>
					<input type="text" id="name" class="form-control" required name="name">
				</div>

				<div class="form-group">
					<label for="email">Correo electrónico</label>
					<input type="email" id="email" class="form-control" required name="email">
				</div>

				<div class="form-group">
					<label for="subject">Asunto</label>
					<input type="text" id="subject" class="form-control" required name="subject">
				</div>

				<div class="form-group">
					<label for="message">Mensaje</label>
					<textarea id="message" class="form-control" required name="message" rows="7"></textarea>
				</div>
			
			</form>
		</div>
	</div>
</div>
