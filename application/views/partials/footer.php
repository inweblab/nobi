<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<p>
					Nodo Binacional en Manufactura
					Avanzada y Procesos Industriales
				</p>
				
				<p>
					Parque San Fandila s/n, Pedro Escobedo, 76703
					Santiago de Querétaro, Qro.
				</p>

				<p>
					Tel +52 (442) 2116000 ext 7840
					<br>
					Whatsapp: 442 364 5725
				</p>
			</div>
			<div class="col-md-8 text-right">
				<img src="/img/layout/logos/logo_nobi_white.png" alt="NoBi">
			</div>
		</div>
	</div>
</footer>
