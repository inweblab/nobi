<!doctype html>
<html lang="es_MX">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Nobi</title>
	<link href="/bower_components/animate.css/animate.min.css" rel="stylesheet">
	<link href="/css/styles.css" rel="stylesheet">
	
	<?php foreach($css as $style): ?>
	<link href="<?php echo $style; ?>" rel="stylesheet">
	<?php endforeach;?>
</head>
