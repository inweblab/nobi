<div class="container-fluid navbar-navbar fixed-top paddingless">
	<div class="container-fluid" id="top-bar">
		<div class="row">
			<div class="col-sm-7">
				<div class="row top-bar">
					<div class="col logo text-center align-self-center p-t-10">
						<img class="align-middle" src="/img/layout/logos/bn/gob_mexico.png" alt="Gobierno de México">
					</div>
					<div class="col logo text-center align-self-center p-t-10">
						<img class="align-middle" src="/img/layout/logos/bn/conacyt.png" alt="Conacyt">
					</div>
					<div class="col logo text-center align-self-center p-t-10">
						<img class="align-middle" src="/img/layout/logos/bn/icorps.png" alt="Icorps">
					</div>
					<div class="col logo text-center align-self-center p-t-10">
						<img class="align-middle" src="/img/layout/logos/bn/nobi_face.png" alt="Nobi">
					</div>
					<div class="col logo text-center align-self-center p-t-10">
						<img class="align-middle" src="/img/layout/logos/bn/nobi_map.png" alt="Nobi">
					</div>
				</div>
			</div>
			<div class="col-sm-5 d-none d-sm-block">
				<nav class="social">
					<ul class="nav justify-content-end">
						<li class="nav-item align-middle">
							<a href="https://www.facebook.com/NobiMAP/" class="fb" target="_blank"></a>
						</li>    
						<li class="nav-item">
							<a href="https://twitter.com/nobimap" class="twitter" target="_blank"></a>
						</li>
						<li class="nav-item">
							<a href="https://mx.linkedin.com/company/nobi-map" class="linkedin" target="_blank"></a>
						</li>
						<li class="nav-item">
							<a href="https://www.youtube.com/channel/UCpM3XsucirrRbAR4T5QGEbA" class="youtube" target="_blank"></a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<div class="container-fluid paddingless shadow bg-white">
		<nav class="navbar navbar-expand-lg navbar-light" id="el-menu">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-center" id="main-menu">
				<ul class="navbar-nav menu_list">
					<li class="nav-item <?php echo $tab=='home'?'active':''; ?>">
						<a href="/" class="nav-link">
							Inicio
						</a>
					</li>
					<li class="nav-item <?php echo $tab=='convocatoria'?'active':''; ?>">
						<a href="/convocatoria" class="nav-link">
							Convocatoria
						</a>
					</li>
					<li class="nav-item <?php echo $tab=='participantes'?'active':''; ?>">
						<a href="/instituciones-participantes" class="nav-link">
							Instituciones participantes
						</a>
					</li>
					<li class="nav-item <?php echo $tab=='lideres'?'active':''; ?>">
						<a href="/lideres" class="nav-link">
							Líderes NoBi MAP
						</a>
					</li>
					<li class="nav-item <?php echo $tab=='casos'?'active':''; ?>">
						<a href="/casos-de-exito" class="nav-link">
							Casos de éxito
						</a>
					</li>
					<li class="nav-item <?php echo $tab=='contacto'?'active':''; ?>">
						<a href="/contacto" class="nav-link">
							Contacto
						</a>
					</li>
					<li class="d-block d-sm-none">
					
						<nav class="social">
							<ul class="list-unstyled">
								<li class="nav-item align-middle">
									<a href="https://www.facebook.com/NobiMAP/" class="fb" target="_blank"></a>
								</li>    
								<li class="nav-item">
									<a href="https://twitter.com/nobimap" class="twitter" target="_blank"></a>
								</li>
								<li class="nav-item">
									<a href="https://mx.linkedin.com/company/nobi-map" class="linkedin" target="_blank"></a>
								</li>
								<li class="nav-item">
									<a href="https://www.youtube.com/channel/UCpM3XsucirrRbAR4T5QGEbA" class="youtube" target="_blank"></a>
								</li>
							</ul>
						</nav>
						
					</li>
				</ul>
			</div>
		</nav>
	</div>

</div>
